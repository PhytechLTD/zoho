<?php
include_once("config.php");
include_once("helpers.php");
include_once("update_zoho.php");
include_once("../google_analytics_zoho_integration/index.php");

add_seconds_to_timeout(1);

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
write_to_log('<br><br>**********************************' . date("Y_m_d H:i:s A") . '***************************************<br><br>' . 'actual_link = ' . $actual_link . '<br>');

$phytech_arr =  [];
$phytech_arr['api_token'] = $GLOBALS['phytech_admin_token'];
$phytech_arr['user_id'] = $GLOBALS['phytech_admin_id'];

//$phytech_response = httpPost($GLOBALS['server_string'] . 'activeadmin/remote_loggers/55050/tested.json', $phytech_arr, 'POST', null);
//$phytech_response = httpPost($GLOBALS['server_string'] . 'activeadmin/remote_loggers/55050/activate.json', $phytech_arr, 'PUT', null);

$request_from = $_GET['SOURCE'];
if (!empty($request_from)) {
    dispatcher();
}
else if (array_key_exists("ID", $_GET)){
    $id = $_GET["ID"];

    if (empty($id) || !is_numeric($id)) {
        die("Error: Id is not valid");
    } else {
        $GLOBALS['Zoho_id'] = $id;
    }

    $module = $_GET["Module_Name"];
    if (empty($module)) {
        die("Error: Module_Name is not valid");
    }
    $method = $_GET["Method"];
    if (empty($module)) {
        die("Error: Method is not valid");
    }

    if ($module == 'Receipts') {
        $module = 'Invoices';//receipt and invoice are the same
    }
    $_phytech_id = $_GET["phytech_id"];
    if (empty($_phytech_id)) {
        $_phytech_id = '-1';
    }
    $req = 'https://crm.zoho.com/crm/private/xml/' . $module . '/getRecordById?authtoken=' . $GLOBALS['authToken'] . '&scope=crmapi&id=' . $id . '&parentModule=' . $module;
    debug_log('req = ' . $req . '<br>');
    //get data from file
    $xml_from_web = simplexml_load_file($req);//load php file from web

    $element_to_append = convertXmlToDOMDocument($xml_from_web, $module);
    if ($module == "Accounts" || $module == "Contacts" || is_not_active_product($module, $method)) {
        post_entity($element_to_append, $module, $method, $id, $_phytech_id);
    }

    if ($module == "Accounts" || $module == "Invoices" || is_active_product($module, $method)) {
        update_xml_for_priority($module, $_phytech_id, $element_to_append, $xml_from_web);
    }
}
else
{
    debug_log("NO ID AND SOURCE IN URL REQUEST");
}
write_to_log("The script was successfully completed");
?>
