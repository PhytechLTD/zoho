<?php
//$file="xml/res_result.xml";
$fileTosave = "xml/temp/res_result_" . date("Y_m_d_H_i") . ".xml";
$fileToNewsave = "xml/temp/res_result_new_" . date("Y_m_d_H_i") . ".xml";

$priorityToken = "123456";
//ini_set("display_errors","off");
//validate xml function

$GLOBALS['authToken'] = '4b8873e7a46d5cf77a027bda4feb3fe4';
$GLOBALS['server_string'] = 'https://plantbeat.phytech.com/';//tesla
$GLOBALS['phytech_admin_id'] = '1091‏';
$GLOBALS['phytech_admin_token'] = 'FrAnazu5rt67';
$GLOBALS['credentials'] = 'user_id=1091&api_token=FrAnazu5rt67';
$GLOBALS['account_temp'] = '10000000448055';
$GLOBALS['default_company_name'] = 'temp';
$GLOBALS['log_path'] = "log.html";
$GLOBALS['log_loading_alerts'] = "log_loading_alerts.html";

function debug_log($str)
{
    echo integration_message($str);
}

function write_to_log($str)
{
    //if (!$GLOBALS['log_file'])
    //{
    //    $GLOBALS['log_file'] = fopen($GLOBALS['log_file_path'], "a");
    //}
    $message = integration_message($str);
    echo $message;
    $f = fopen($GLOBALS['log_path'], 'a');
    fwrite($f,$message);
    fclose($f);
}

function write_to_loading_alerts_log($str)
{
    $message = integration_message($str);
    echo $message;
    $f = fopen($GLOBALS['log_loading_alerts'], 'a');
    fwrite($f,$message);
    fclose($f);

}

/*
function close_log_file()
{
    if (array_key_exists('log_file', $GLOBALS) && !$GLOBALS['log_file'])
    {
        fclose($GLOBALS['log_file']);
    }
}
*/
function validateXML($xmlstr)
{
    $xml = new XMLReader();
    if ($xml->xml($xmlstr, NULL, LIBXML_DTDVALID)) {
        return true;
    }
    return false;
}

//convert xml to array   
function XML2Array($xmlString)
{
    return (array)simplexml_load_string($xmlString);
}

function p($arr)
{
    debug_log("<pre>");
    print_r($arr);
    exit;
}

function post_entity($entity, $type, $method, $id, $_phytech_id)
{
    //$authToken = "0cf560493433b272f5c52dd3e16fd158";
    $phytech_arr = [];
    if($_phytech_id == '-1') {
        $phytech_arr = convertXmlToArr($entity, $type);
    }
    $phytech_arr['api_token'] = $GLOBALS['phytech_admin_token'];
    $phytech_arr['user_id'] = $GLOBALS['phytech_admin_id'];
    $post_params_zoho = array();
    $post_params_zoho['authtoken'] = $GLOBALS['authToken'];
    debug_log('printing phytech_arr:<br>');
    foreach ($phytech_arr as $x => $x_value) {
        debug_log("Key=" . $x . ", Value=" . $x_value);
        if ($x == "user") {
            $user_arr = $x;
        }
        debug_log("<br>");
    }
    foreach ($user_arr as $x => $x_value) {
        debug_log("Key=" . $x . ", Value=" . $x_value);
        debug_log("<br>");
    }
    $zoho_id = $id;
    //check if new entity in zoho is already defined in phytech
    if ($type == "Accounts" || $type == "Contacts") {
        $entity_exist = is_entity_exist_in_phytech($phytech_arr['user'], $type);
        debug_log('entity_exist= '.$entity_exist.'<br>');
        debug_log('method= '.$method.'<br>');
        debug_log('zoho_id = ' . $zoho_id . '<br>');
        $user = $phytech_arr['user'];
        $user_id = $user['id'];
        if ($entity_exist && $method == 'Edited') {
            write_to_log('going to patch. user_id=' . $user_id . '<br>');
            $user_array_from_phytech = get_phytech_user_json_array_by_id($user_id);
            $changed_fields = getDifferentFields($user, $user_array_from_phytech);
            $changed_fields['lock_version'] = $user_array_from_phytech['lock_version'];
            $phytech_arr['user'] = $changed_fields;
            $phytech_response = httpPost($GLOBALS['server_string'] . '/activeadmin/users/' . $user_id . '.json', $phytech_arr, 'PATCH', null);
            if ((strpos($phytech_response, "error_") === false)) {
                write_to_log('user with phytech id :' . $user_id . ' have been updated successfully<br>');
                updateZohoAboutPhytechStatus($zoho_id, $type, '',$user_id);
                write_to_log($phytech_response);
            } else {
                write_to_log('user with phytech id :' . $user_id . ' have not been updated successfully<br>');
                write_to_log('The response from phytoweb: ' . $phytech_response . '<br>');
                updateZohoAboutPhytechStatus($zoho_id, $type, $phytech_response,$user_id);
                write_to_log($phytech_response);
            }
        } else {
            if(!$entity_exist && $method == 'Edited'){
             //in this case we are trying to  edit a Contact or Company in zoho of a none existing user or group in phytech
            // the solution is to post user as if it is newly created into phytech
                create_new_user_in_phytech($phytech_arr,$type,$zoho_id,$entity);
            }else
            if ($method == 'updated') {
                die("Error: Can not update entity that does not exist in phytech");
            } else {
                if ($method == 'Created') {
                    debug_log('method = Created<br>');
                    create_new_user_in_phytech($phytech_arr,$type,$zoho_id,$entity);
                } else {
                    if ($method == 'Deleted') {
                        $phytech_response = httpPost($GLOBALS['server_string'] . '/activeadmin/users/' . $_phytech_id . '.json', $phytech_arr, 'DELETE', null);
                        debug_log('phytech response = ' . $phytech_response . '<br>');
                    }
                }
            }
        }
    } else {
        if ($type == 'Products')
            if ($method == 'Edited' || $method == 'Created') {
                $logger_id = $entity->getElementsByTagName('Product_Code')->item(0)->nodeValue;
                debug_log('logger_id = ' . $logger_id . '<br>');
                $remote_logger = [];
                debug_log('need to connect logger to  user' . '<br>');
                $zoho_owner_id = $entity->getElementsByTagName('Company_ID')->item(0)->nodeValue;
                $url = 'https://crm.zoho.com/crm/private/xml/Accounts/getRecordById?authtoken=' . $GLOBALS['authToken'] . '&scope=crmapi&id=' . $zoho_owner_id . '&parentModule=Accounts';
                debug_log('url to get target account from zoho: '.$url);
                $xml_from_web = simplexml_load_file($url);//load php file from web
                $zoho_owner = convertXmlToDOMDocument($xml_from_web, 'Accounts');
                $phytech_id = $zoho_owner->getElementsByTagName('Phytech_ID')->item(0)->nodeValue;
                debug_log('phytech_id from zoho  = ' . $phytech_id . '<br>');
                $remote_logger['owner_id'] = $phytech_id;
                $phytech_arr['remote_logger'] = $remote_logger;
                $phytech_response = httpPost($GLOBALS['server_string'] . '/activeadmin/remote_loggers/' . $logger_id . '.json', $phytech_arr, 'PATCH', null);
            }
            else if ($method == ENDPOINTS::ACTIVATE_PRODUCT) {
                $phytech_arr['remote_logger'] = $remote_logger;
                $phytech_response = httpPost($GLOBALS['server_string'] . '/activeadmin/remote_loggers/' . $logger_id . '.json', $phytech_arr, 'PATCH', null);

                if (strpos($phytech_response, "error_") === false) {
                    write_to_log('logger #' . $logger_id . ' has changed owner successfully<br>');
                } else {
                    write_to_log('logger #' . $logger_id . ' has not changed owner successfully :-(<br>');
                }
            }
            updateZohoAboutPhytechStatus($zoho_id, $type, $phytech_response, $logger_id);

        //write_to_log('phytech response: '.$phytech_response);
    }
}




function create_new_user_in_phytech($phytech_arr,$type,$zoho_id,$domNode){
    $phytech_arr['id'] = null;
    $user = $phytech_arr['user'];
    //creating user in phytech. if type is account, user.type = 'group'
    $phytech_response = httpPost($GLOBALS['server_string'] . '/activeadmin/users.json', $phytech_arr, 'POST', null);
    write_to_log('phytech response = ' . $phytech_response . '<br>');
    if ((strpos($phytech_response, "error_") === false)) {
        //got response 200 ok from phytech. user has been created in phytech.
        write_to_log('created user in phytech successfully' . '<br>');
        $email = $user['email'];
        $phytech_id = get_phytech_user_by_email($email);
        debug_log('phytech_id = ' . $phytech_id . '<br>');
        $zoho_response = updateZoho($phytech_id, $zoho_id, $type);
        $account_id = $domNode->getElementsByTagName('ACCOUNTID')->item(0)->nodeValue;
        debug_log('account_id = '.$account_id.'<br>');
        if(is_null($account_id)){
            $account_id = $GLOBALS['account_temp'];
            debug_log('account_id = '.$account_id.'<br>');
            updateZohoCompanyName($GLOBALS['default_company_name'],$zoho_id,'Contacts');
        }

        updateZohoAboutPhytechStatus($zoho_id, $type, '',$phytech_id);
        write_to_log('zoho response  = ' . $zoho_response . '<br>');
        write_to_log($phytech_response);
    } else {
        write_to_log('user have not been created successfully in phytech<br>');
        updateZohoAboutPhytechStatus($zoho_id, $type, $phytech_response,'-1');
        write_to_log($phytech_response);
    }

}
function is_entity_exist_in_phytech($phytech_arr, $type)
{
    debug_log('in is_entity_exist_in_phytech<br>');
    $res = false;
    switch ($type) {
        case "Accounts":
            $id = $phytech_arr['id'];
            $user_id = get_phytech_user_by_id($id);
            if ($user_id != null) {
                $res = true;
            }
            break;
        case "Products":

            break;
        case "Contacts":
            $id = $phytech_arr['id'];
            $user_id = get_phytech_user_by_id($id);
            if ($user_id != null) {
                $res = true;
            }
            break;
        default:
            write_to_log("type = " . $type);
    }
    debug_log('res = '.$res.'<br>');
    debug_log('out of is_entity_exist_in_phytech');
    return $res;

}

function convertXmlToArr($domNode, $type)
{
    $user_convert_map = array(//a map from zoho contact to phytech user_error
        "Last_Name" => "last_name",
        "First_Name" => "first_name",
        "Phone" => "phone_number",
        "Created_Time" => "created_at",
        "Modified_Time" => "updated_at",
        "Billing_Street" => "address",
        "Billing_Country" => "country",
        "Email" => "email",
        "Phytech_ID" => "id",
        "Country." => "country",
        "metric_units" => "metric_units",
        "Logo_file_path" => "logo"
    );

    $res = [];
    switch ($type) {
        case "Accounts":
            $user = prepere_user($domNode, $user_convert_map,$type);
            $user['type'] = 'Group';//companies are groups in phytech
            $res['user'] = $user;
            break;
        case "Products":
            break;
        case "Contacts":
            $user = prepere_user($domNode, $user_convert_map,$type);
            $res['user'] = $user;
            break;
        default:
            write_to_log("type = " . $type);
    }
    if ($type == "Accounts" || $type == "Contacts") {
        debug_log('print user params:<br>');
        foreach ($user as $k => $v) {
            debug_log('key = ' . $k . ' value = ' . $v . '<br>');
        }
    }
    return $res;

}

function prepere_user($domNode, $user_convert_map,$type)
{
    write_to_log('in prepere_user' . '<br>');
    $user = convert_entity_to_phytech($domNode, $user_convert_map);
    $user['password'] = '12345678';
    if($type == 'Accounts') {
        $agronomist_id = get_agronomist_or_technician_of_user($domNode, 'Agronomist');
        if ($agronomist_id == null){$agronomist_id = 724;};
        $user['agronomist_id'] = $agronomist_id;
        $user['technician_id'] = get_agronomist_or_technician_of_user($domNode, 'Technician');//set agronomist_id from phytech
        $user['first_name'] = $domNode->getElementsByTagName('Account_Name')->item(0)->nodeValue;
        $user['last_name'] = $domNode->getElementsByTagName('Account_Name')->item(0)->nodeValue;
        $client_type = $domNode->getElementsByTagName('client_type')->item(0)->nodeValue;
        if(!empty($client_type)){
            $user['role'] = str_replace(' ', '_', strtolower($client_type));
        }
    }
    if($type == 'Contacts'){
        $user['agronomist_id'] = 724;
        $user['technician_id'] = 742;
        $user['country'] = $domNode->getElementsByTagName('Country.')->item(0)->nodeValue;
        $account_id = $domNode->getElementsByTagName('ACCOUNTID')->item(0)->nodeValue;
        debug_log('account_id = '.$account_id.'<br>');
        if(is_null($account_id)){
            $account_id = $GLOBALS['account_temp'];
        }
        debug_log('account_id = '.$account_id.'<br>');
        $zoho_owner_id = $GLOBALS['prefix_zoho_id'].$account_id;
        debug_log('zoho_owner_id = '.$zoho_owner_id.'<br>');
        $url = 'https://crm.zoho.com/crm/private/xml/Accounts/getRecordById?authtoken=' . $GLOBALS['authToken'] . '&scope=crmapi&id=' . $zoho_owner_id . '&parentModule=Accounts';
        debug_log('url = '.$url . '<br>');
        $xml_from_web = simplexml_load_file($url);//load php file from web
        $zoho_owner = convertXmlToDOMDocument($xml_from_web, 'Accounts');
        $phytech_id = $zoho_owner->getElementsByTagName('Phytech_ID')->item(0)->nodeValue;
        debug_log('phytech_id from zoho  = ' . $phytech_id . '<br>');
        $user['group_id'] = $phytech_id;
    }
    $metric_units = $domNode->getElementsByTagName('metric_units')->item(0)->nodeValue;
    $user['metric_units'] = 1;
    if ($metric_units == 'inches') {
        $user['metric_units'] = 0;
    }
    return $user;
}


function get_agronomist_or_technician_of_user($domNode, $type)
{
    debug_log('get_agronomist_or_technician_of_user('.$type.')');
    if ($type == 'Agronomist') {
        switch ($_GET['COMPANY_OWNER_ID']) {
            case '1180010000001563161':
                $email = 'alon.v@phytech.com';
                break;
            case '1180010000006012001':
                $email = 'bill.d@phytech.com';
                break;
            case '1180010000000500087':
                $email = 'brian.b@phytech.com';
                break;
            case '1180010000000445181':
                $email = 'liraz@phytech.com';
                break;
            case '1180010000003065199':
                $email = 'nir.b@phytech.com';
                break;
            case '1180010000005829001':
                $email = 'ofir.n@phytech.com';
                break;
            case '1180010000001563171':
                $email = 'omer.s@phytech.com';
                break;
            case '1180010000001770013':
                $email = 'yifat.t@phytech.com';
                break;
            case '1180010000000068001':
                $email = 'yochay@phytech.com';
                break;
            case '1180010000006610387':
                $email = 'yuval.k@phytech.com';
                break;
            case '1180010000008350042':
                $email = 'jeff.a@phytech.com';
                break;
            case '1180010000008671105':
                $email = 'tom.z@phytech.com';
                break;
	    case '1180010000009588017':
		$email = 'mark.h@phytech.com';
		break;
            default:
                $email = 'system@phytech.com';
        }
    }
    else if ($type == 'Technician')
    {
        //1.get SMOWNERID out of $domNode
        $zoho_agronomist_field = $domNode->getElementsByTagName($type)->item(0)->nodeValue;
        debug_log('zoho_agronomist_field = ' . $zoho_agronomist_field . '<br>');
        $pieces = explode(":", $zoho_agronomist_field);
        $email = $pieces[1];
        $email = trim($email, ' ');
        debug_log('in get_agronomist_of_user. email = ' . $email . '<br>');
        //$email  = $domDocument->getElementsByTagName('Email')->item(0)->nodeValue;
        //4. get phytech user id by email
    }

    debug_log('in get_agronomist_or_technician_of_user. email = ' . $email . '<br>');

    debug_log('in get_agronomist_of_user <br>');
    debug_log('email = '.$email.'<br>');
    $phytech_agronomist_id = get_phytech_user_by_email($email);
    debug_log('phytech_agronomist_id = ' . $phytech_agronomist_id . '<br>');
    return $phytech_agronomist_id;
}

function convertXmlToDOMDocument($xml, $module, $dont_cut_id = false)
{
    if (gettype($xml) == "boolean"){
        return null;
    }
    else {
        //gets a simpleXml element and returns a DOMDocument with relevant info. much easier to work with
        $xml_from_web_string = $xml->asXML();//convert php file from web to string
        $xml_from_web_string_converted = xmlConvertAttributes($xml_from_web_string, $module, $dont_cut_id);
        $xml_from_web_string_converted = str_replace('&', ' and ', $xml_from_web_string_converted);
        $doc_from_web = new DOMDocument();
        //debug_log('type of $xml_from_web_string: '.gettype($xml_from_web_string));
        //debug_log('$xml_from_web_string_converted: '.$xml_from_web_string_converted);
        $doc_from_web->loadXML($xml_from_web_string_converted);
        $rows_from_web = $doc_from_web->getElementsByTagName('row');
        debug_log('type of rows_from_web: '.gettype($rows_from_web));
        $res = $rows_from_web->item(0);
        return $res;
    }
}

function get_phytech_user_by_email($email)
{
    $email = trim($email, ' ');
    $email = strtolower($email);
    debug_log('in get_phytech_user_by_email. email = ' . $email . '<br>');
    $res = null;
    $url = $GLOBALS['server_string'] . 'activeadmin/users.json?'.$GLOBALS['credentials'].'&utf8=%E2%9C%93&q%5Bemail_equals%5D=' . $email . '&commit=Filter&order=id_asc';
    debug_log('url = ' . $url . ' <br>');
    $user_from_phytech = file_get_contents($url);
    //write_to_log('user_from_phytech = ' . $user_from_phytech . '<br>');
    $arr = json_decode($user_from_phytech);
    if (count($arr)) {
        $res = $arr[0]->id;
    }
    debug_log('user_from_phytech = '.$user_from_phytech.'<br>');
    debug_log('out of get_phytech_user_by_email. res = ' . $res . '<br>');
    return $res;
}

function get_phytech_user_by_id($id)
{
    if ($id == 0) {
        debug_log("phytech id by zoho is 0");
        return null;
    }
    $id = trim($id, ' ');
    $res = null;
    $url = $GLOBALS['server_string'] . '/activeadmin/users/' . $id . '.json?'.$GLOBALS['credentials'];
    write_to_log('url = ' . $url . ' <br>');
    $user_from_phytech = file_get_contents($url);
    $user = json_decode($user_from_phytech);
    $res = $user->id;
    debug_log('in get_phytech_user_by_id. res = ' . $res . 'id = ' . $id . '<br>');
    return $res;
}

function get_phytech_user_json_array_by_id($id)
{
    $id = trim($id, ' ');
    $res = null;
    $url = $GLOBALS['server_string'] . '/activeadmin/users/' . $id . '.json?'.$GLOBALS['credentials'];
    write_to_log('getting user json... url = ' . $url . ' <br>');
    $user_from_phytech = file_get_contents($url);
    $user = json_decode($user_from_phytech, true);
    $res = $user;
    debug_log('in get_phytech_user_json_array_by_id. <br>');
    return $res;
}

function convert_entity_to_phytech($domNode, $trans_map)
{
    debug_log('in convert_entity_to_phytech' . '<br>');
    $res = array();
    foreach ($domNode->childNodes as $node) {
        $phytech_attribute = $trans_map[(string)$node->nodeName];
        debug_log('zoho attr = ' . $node->nodeName . '<br>');
        debug_log('zoho value = ' . $node->nodeValue . '<br>');
        debug_log('phytech attr = ' . $phytech_attribute . '<br>');
        if ($phytech_attribute) {
            if(is_null($res[$phytech_attribute])) {
                $res[$phytech_attribute] = $node->nodeValue;
            }
        }

    }
    write_to_log('res = <br>');
    foreach ($res as $k => $v) {
        debug_log('res [' . $k . '] = ' . $v . '<br>');
    }
    debug_log('out of convert_entity_to_phytech <br>');
    return $res;
}

function xml_join($root, $append)
{
    if ($append) {
        if (strlen(trim((string)$append)) == 0) {
            $xml = $root->addChild($append->getName());
            foreach ($append->children() as $child) {
                xml_join($xml, $child);
            }
        } else {
            $xml = $root->addChild($append->getName(), (string)$append);
        }
        foreach ($append->attributes() as $n => $v) {
            $xml->addAttribute($n, $v);
        }
    }
}

function xmlConvertAttributes($note, $module, $dont_cut_id = false)
{
    $xml = new SimpleXMLElement($note);
    $strRow = "";
    if (isset($xml->result->$module) && isset($xml->result->$module->row)) {
        foreach ($xml->result->$module->row as $row) {
            $strRow .= "<row>";
            $temp = (array)$row->attributes()->no;
            $strRow .= "<no>" . $temp[0] . "</no>";
            $totalRow = 0;
            foreach ($row->FL as $fl) {
                $temp = (array)$fl->attributes()->val;
                $name = $temp[0];
                //debug_log('$name of attribute: '.$name);
                if ($name == "Product Details") {
                    $strRow .= "<Product_Details>";
                    if (isset($fl->product)) {
                        foreach ($fl->product as $product) {
                            $strRow .= "<product>";
                            $temp = (array)$product->attributes()->no;
                            $strRow .= "<no>" . $temp[0] . "</no>";
                            $total = 0;
                            foreach ($product->FL as $prFl) {
                                $temp = (array)$prFl->attributes()->val;
                                $name = $temp[0];
                                $name = str_replace(" ", "_", $name);
                                $value = (string)$prFl;
                                if ($name == "Total") {
                                    $total = $value;
                                }
                                if ($name == "Product_Name")
                                {
                                    $value = str_replace("&"," and ", $value);
                                }
                                if ($name == "Discount") {
                                    if ($total) {
                                        $value = floatval($value) / floatval($total) * 100;
                                    }
                                }

                                $strRow .= "<" . $name . ">" . $value . "</" . $name . ">";
                            }
                            $strRow .= "</product>";
                        }
                    }
                    $strRow .= "</Product_Details>";
                }
                else {
                    $temp = (array)$fl->attributes()->val;
                    $name = $temp[0];
                    $name = str_replace(" ", "_", $name);
                    $value = (string)$fl;

                    if ($dont_cut_id != true)
                    {
                        if (in_array($name, array("INVOICEID", "Invoice_Number", "SALESORDERID", "ACCOUNTID", "SMOWNERID", "SMCREATORID", "MODIFIEDBY", "SO_Number","CONTACTID"))) {
                            if ($name == 'ACCOUNTID' || $name == 'INVOICEID') {
                                $GLOBALS['prefix_zoho_id'] = substr($value, 0, -14);
                                write_to_log('saving prefix_zoho_id: '.$GLOBALS['prefix_zoho_id']);
                            }
                            $value = getSubstring($value, 14);
                        }
                    }

                    if ($name == "Sub_Total") {
                        $totalRow = $value;
                    }
                    if ($name == "Discount") {
                        if ($totalRow) {
                            $value = floatval($value) / floatval($totalRow) * 100;
                        }
                    }

                    if ($name == "Product_Name"){
                        $value = str_replace("&"," and ", $value);
                    }


                    if ($name == 'Billing_Country.'){
                        $name = 'Billing_Country';
                    }

                    if (strpos($name, '(Minutes)') !== false)
                    {
                        debug_log("skipping the field 'Average Time Spent (Minutes)'");
                        break;
                    }

                    $strRow .= "<" . $name . ">" . $value . "</" . $name . ">";
                    //debug_log('strRow: '.$strRow);
                }
            }
            $strRow .= "</row>";
        }
    }
    return <<<XML
    <response>
    <uri>/crm/private/xml/{$module}/getRecordById</uri>
    <result>
      <{$module}>
      {$strRow} 
      </{$module}>
    </result>
    </response>
XML;
}

function getSubstring($value, $length)
{
    if (strlen($value) > $length) {
        $value = substr($value, strlen($value) - $length, strlen($value));
    }
    return $value;
}

function httpPost($url, $params, $method, $cookies)
{
    //get cookie

    $postData = '';
    $remote_logger_params = null;
    $user_params = null;
    //create name value pairs seperated by &
    if ($params) {
        foreach ($params as $k => $v) {
            if ($k == 'user') {
                $user_params = $v;
            } else {
                if ($k == 'remote_logger') {
                    $remote_logger_params = $v;
                } else {
                    $postData .= $k . '=' . $v . '&';
                }
            }
        }
    }
    if ($user_params) {
        foreach ($user_params as $k => $v) {
            $postData .= 'user[' . $k . ']' . '=' . $v . '&';
        }
    }
    if ($remote_logger_params) {
        foreach ($remote_logger_params as $k => $v) {
            $postData .= 'remote_logger[' . $k . ']' . '=' . $v . '&';
        }
    }
    rtrim($postData, '&');
    write_to_log('in httpPost. url = ' . $url . '<br>');
    write_to_log('postData = ' . $postData . '<br>');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    if ($method == 'PATCH') {
        write_to_log('going to patch<br>');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
    } else {
        if ($method == 'DELETE') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        }
        else if ($method == 'PUT') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        }

    }
    $output = curl_exec($ch);
    if (curl_errno($ch)) {
        write_to_log('output = ' . $output . '<br>');
        $output = 'error_' . curl_errno($ch) . '_' . $output;
        write_to_log('error accured in post http ' . curl_errno($ch) . '<br>');
    }
    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    write_to_log($http_status . '<br>');
    if ($http_status >= 400) {
        if (strpos($output,"The page you were looking for doesn't exist") !== false) {
            $output = "The page you were looking for doesn't exist";
        }
        write_to_log('output = ' . $output . '<br>');
        $output = 'error_' . $http_status . '_' . $output;
    }
    debug_log('out of httpPost' . '<br>');
    return $output;
}


function get_cookies()
{
    $ch = curl_init('http://178.79.155.23:3000/users/sign_in');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // get headers too with this line
    curl_setopt($ch, CURLOPT_HEADER, 1);
    $result = curl_exec($ch);
    // get cookie
    preg_match('/^Set-Cookie:\s*([^;]*)/mi', $result, $m);
    parse_str($m[1], $cookies);
    var_dump($cookies);
    return $cookies;
}

function updateZoho($phytech_id, $zoho_id, $module)
{
    //$authtoken = '0cf560493433b272f5c52dd3e16fd158';
    //the actual data
    write_to_log('in updateZoho' . '<br>');
    //$req = 'https://crm.zoho.com/crm/private/xml/'.$module.'/getRecordById?authtoken='.$authtoken.'&scope=crmapi&id='.$zoho_id.'&parentModule='.$module;
    $xml = '<Accounts><row no="1"><FL val="Phytech ID">' . $phytech_id . '</FL></row></Accounts>';
    $postData = 'authtoken=' . $GLOBALS['authToken'] . '&scope=crmapi&id=' . $zoho_id . '&xmlData=' . $xml;
    $req_for_post = 'https://crm.zoho.com/crm/private/xml/' . $module . '/updateRecords?';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_URL, $req_for_post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);
    write_to_log('response from zoho : ' . $output . '<br>');
    if (curl_errno($ch)) {
        //$output = 'error';
        write_to_log('error accured in post http ' . curl_errno($ch) . '<br>');
    }
    curl_close($ch);
    debug_log('zoho_id = ' . $zoho_id . '<br>');
    debug_log('xml = ' . $xml . '<br>');
    debug_log('phytech_id = ' . $phytech_id . '<br>');
    write_to_log('req sent as post to zoho = ' . $req_for_post . $postData . '<br>');
    debug_log('out of updateZoho ' . '<br>');
    return $output;
}


function updateZohoCompanyName($company_name, $zoho_id, $module)
{
    //update zoho company name for contact with zoho id
    debug_log('in updateZohoCompanyName' . '<br>');
    //$req = 'https://crm.zoho.com/crm/private/xml/'.$module.'/getRecordById?authtoken='.$authtoken.'&scope=crmapi&id='.$zoho_id.'&parentModule='.$module;
    $xml = '<Accounts><row no="1"><FL val="Account Name">' . $company_name . '</FL></row></Accounts>';
    $postData = 'authtoken=' . $GLOBALS['authToken'] . '&scope=crmapi&id=' . $zoho_id . '&xmlData=' . $xml;
    $req_for_post = 'https://crm.zoho.com/crm/private/xml/' . $module . '/updateRecords?';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_URL, $req_for_post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);
    write_to_log('response from zoho : ' . $output . '<br>');
    if (curl_errno($ch)) {
        //$output = 'error';
        write_to_log('error accured in post http ' . curl_errno($ch) . '<br>');
    }
    curl_close($ch);
    debug_log('zoho_id = ' . $zoho_id . '<br>');
    debug_log('xml = ' . $xml . '<br>');
    debug_log('company_name = ' . $company_name . '<br>');
    debug_log('req sent as post to zoho = ' . $req_for_post . $postData . '<br>');
    debug_log('out of updateZoho ' . '<br>');
    return $output;
}

function update_zoho_field($value, $zoho_id, $module,$field_name)
{
    //update zoho module in  field_name with value
    debug_log('in updateZohoField' . '<br>');
    $xml = '<'.$module.'><row no="1"><FL val="'.$field_name.'">' . $value . '</FL></row></'.$module .'>';
    $postData = 'authtoken=' . $GLOBALS['authToken'] . '&scope=crmapi&id=' . $zoho_id . '&xmlData=' . $xml;
    $req_for_post = 'https://crm.zoho.com/crm/private/xml/' . $module . '/updateRecords?';
    debug_log("url: ".$req_for_post. " postData: ".$postData);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_URL, $req_for_post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);
    write_to_log('response from zoho : ' . $output . '<br>');
    if (curl_errno($ch)) {
        //$output = 'error';
        write_to_log('error accured in post http ' . curl_errno($ch) . '<br>');
    }
    curl_close($ch);
    debug_log('zoho_id = ' . $zoho_id . '<br>');
    debug_log('xml = ' . $xml . '<br>');
    debug_log('value = ' . $value . '<br>');
    write_to_log('req sent as post to zoho = ' . $req_for_post . $postData . '<br>');
    debug_log('out of update zoho field ' . '<br>');
    return $output;
}
function getDifferentFields($arr1, $arr2)
{
    $res = [];
    debug_log('in getDifferentFields<br>');
    foreach ($arr1 as $k => $v) {
        debug_log('key = ' . $k . '<br>');
        debug_log('v1 = ' . $v . '<br>');
        debug_log('v2 = ' . $arr2[$k] . '<br>');
        if ($arr2[$k] != $v) {
            $res[$k] = $v;
        }
    }
    write_to_log('res = <br>');
    foreach ($res as $k => $v) {
        debug_log('res [' . $k . '] = ' . $v . '<br>');
    }
    debug_log('out of getDifferentFields<br>');
    return $res;
}

function updateZohoAboutPhytechStatus($zoho_id, $module, $phytech_response,$phytech_id)
{
    debug_log('in updateZohoStatus' . '<br>');
    $is_error = 'True';
    if ((strpos($phytech_response, "error_") === false)) {
        $is_error = 'False';
        $phytech_response = 'Phytech ID : '.$phytech_id;
        if ($module == 'Products'){
            $logger_status = get_logger_status_from_plantbeat_json($phytech_id);
            debug_log('after getting logger details from plantbeat');
            debug_log($logger_status->timezone);
            debug_log($logger_status->tested);
            $phytech_response .= $logger_status->tested  ? ", tested" : ", untested";
        }
    }
    else if (strpos($phytech_response, "{\"errors\":{") == true) {
        preg_match("/\{(?<errors>[^\}]*)/", $phytech_response, $matches);
        $formatted_error = str_replace("\"", " ", $matches["errors"]);
        $formatted_error = str_replace("errors :{", "Errors - ", $formatted_error);
        $formatted_error = preg_replace("/[\[\]]/", "", $formatted_error);
        $phytech_response = $formatted_error;
    }

    $xml = '<Accounts><row no="1"><FL val="Phytech Status">' . $phytech_response . '</FL><FL val="Resend">' . ($is_error) . '</FL></row></Accounts>';
    $postData = 'authtoken=' . $GLOBALS['authToken'] . '&scope=crmapi&id=' . $zoho_id . '&xmlData=' . $xml;
    $req_for_post = 'https://crm.zoho.com/crm/private/xml/' . $module . '/updateRecords?';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_URL, $req_for_post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);
    write_to_log('response from zoho : ' . $output . '<br>');
    if (curl_errno($ch)) {
        //$output = 'error';
        write_to_log('error accured in post http ' . curl_errno($ch) . '<br>');
    }
    curl_close($ch);
    write_to_log('req sent as post to zoho = ' . $req_for_post . $postData . '<br>');
    write_to_log('out of updateZohoAboutPhytechStatus ' . '<br>');
    return $output;
}

function integration_message($message)
{
    $return_message = "<br>";
    if (property_exists('$GLOBALS','Zoho_id') && property_exists('$GLOBALS','module')) {
        $return_message += "id: {$GLOBALS['Zoho_id']} module: " . $GLOBALS['module'] . " ";
    }
    return $return_message . date('Y-m-d H:i:s') . ' : ' . $message . "<br>";
}

function createZohoTask($error_message, $entity, $type)
{
    //$authtoken = '0cf560493433b272f5c52dd3e16fd158';
    write_to_log('in createZohoTask' . '<br>');
    $time = date('Y-m-d');
    $owner = $entity->getElementsByTagName('Contact_Owner')->item(0)->nodeValue;

    if ($type == "Accounts" || $type == "Contacts") {
        $zoho_id = $entity->getElementsByTagName('CONTACTID')->item(0)->nodeValue;
        debug_log('zoho_id = ' . $zoho_id . '<br>');
        $row_to_append = '<FL val="CONTACTID">' . $zoho_id . '</FL>';
    } else {
        $product_id = $entity->getElementsByTagName('PRODUCTID')->item(0)->nodeValue;
        $row_to_append = '<FL val="PRODUCTID">' . $product_id . '</FL>';
    }
    $xml = '<Tasks>
     <row no="1">
     <FL val="Subject">' . substr($error_message, 0, 10) . '</FL>
     <FL val="Description">' . $error_message . '</FL>
     <FL val="Due Date">' . $time . '</FL>
     <FL val="Status">High</FL>
     <FL val="Task Owner">' . $owner . '</FL>'
        . $row_to_append .
        '</row>
     </Tasks>';
    $postData = 'newFormat=1&authtoken=' . $GLOBALS['authToken'] . '&scope=crmapi&xmlData=' . $xml;
    $req_for_post = 'https://crm.zoho.com/crm/private/xml/Tasks/insertRecords?';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_URL, $req_for_post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);
    write_to_log('response from zoho : ' . $output . '<br>');
    if (curl_errno($ch)) {
        //$output = 'error';
        write_to_log('error accured in post http ' . curl_errno($ch) . '<br>');
    }
    curl_close($ch);
    write_to_log('req sent as post to zoho = ' . $req_for_post . $postData . '<br>');
    write_to_log('response from zoho :' . $output . '<br>');
    debug_log('out of createZohoTask ' . '<br>');
    return $output;
}

function update_zoho_companies_from_phytech()
{
    $load_records = true;
    $defence = 3;
    $iteration = 0;
    $from_index = 1;
    $block_size = 200;
    $to_index = $block_size;
    while ($load_records == true) {
        $url = 'https://crm.zoho.com/crm/private/xml/Accounts/getMyRecords?newFormat=1&authtoken=' . $GLOBALS['authToken'] . '&scope=crmapi&fromIndex=' . $from_index . '&toIndex=' . $to_index;
        write_to_log('url = ' . $url . '<br>');
        $xml_from_web = simplexml_load_file($url);
        $xml_from_web_string = $xml_from_web->asXML();
        $xml_from_web_string_converted = xmlConvertAttributes($xml_from_web_string, 'Accounts');
        $doc_from_web = new DOMDocument();
        $doc_from_web->loadXML($xml_from_web_string_converted);
        $rows_from_web = $doc_from_web->getElementsByTagName('row');
        debug_log('rows.length = ' . $rows_from_web->length . '<br>');
        for ($i = 0; $i < $rows_from_web->length; $i++) {
            $account = $rows_from_web->item($i);
            $phytech_id = $account->getElementsByTagName('Phytech_ID')->item(0)->nodeValue;

            if ($i == 0) {
                $phytech_user = get_phytech_user_json_array_by_id($phytech_id);
                foreach ($account->childNodes as $node) {
                    debug_log('zoho attr = ' . $node->nodeName . '<br>');
                    debug_log('zoho value = ' . $node->nodeValue . '<br>');
                }
                foreach ($phytech_user as $key => $value) {
                    debug_log("phytech attr: $key; phytech value: $value<br />\n");
                }
            }

        }
        if ($rows_from_web->length < $block_size) {
            $load_records = false;
        }
        $iteration++;
        $from_index = $to_index + 1;
        $to_index = $to_index + $block_size;
        if ($iteration > $defence) {
            $load_records = false;
        }
    }

}
function get_zoho_company($id, $module){
    $req = 'https://crm.zoho.com/crm/private/xml/'.$module.'/getRecordById?authtoken='.$GLOBALS['authToken'].'&scope=crmapi&id='.$id.'&parentModule='.$module;
    write_to_log('req = '.$req.'<br>');
//get data from file
    $xml_from_web = simplexml_load_file($req);//load php file from web
    $res = convertXmlToDOMDocument($xml_from_web,$module);
    return $res;
}

?>