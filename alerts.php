<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/10/2015
 * Time: 11:45
 */

class DATASET_TYPES_IDS {
    const BATTERY = '184';
    const DENDROMETER = '22';
    const STEM_DIAMETER_SENSOR_TYPE_SD_REV2 = 89;
    const STEM_DIAMETER_SENSOR_TYPE_SD_56R = 280;
}

class ALERTS_TYPES_TITLE {
    const NOT_REPORTING = 'Logger not reported';
    const LOW_BATTERY = 'Logger low battery';
    const DENDROMETER_VALUES = 'Invalid dendrometer values';
}

abstract class alert_base
{
    public $alert_subject;
    public static $original_alerts_from_plantbeat = array();

    abstract protected function add_special_data_for_alert_type($logger_status);
    abstract protected function alert_dataset_types_ids();

    public function run()
    {
        write_to_loading_alerts_log("=*=*=*=*=*=*=*=* " . static::alert_subject . " alerts synchronization was started..." . " =*=*=*=*=*=*=*=*");

        $alerted_logger_ids = array();

        foreach (static::alert_dataset_types_ids() as $type_id) {
            write_to_log("getting alerts for dataset type id: " . $type_id);
            $alerted_logger_ids =
                array_merge($alerted_logger_ids, $this->get_logger_ids_by_alerted_dataset_type($type_id, static::alert_subject));
        }

        debug_log('count of alerted logger ids: '. (string)count($alerted_logger_ids));
        write_to_loading_alerts_log("removing non relevant alerts was started...");

        $this->remove_not_relevant_alerts_in_zoho($alerted_logger_ids, static::alert_subject);

        write_to_loading_alerts_log("creating new alerts was started...");

        $this->create_alerts($alerted_logger_ids);

        debug_log('after run run() for '.static::alert_subject.'. alerts');
        write_to_loading_alerts_log("=*=*=*=*=*=*=*=* " . static::alert_subject . " alerts synchronization was finished..." . " =*=*=*=*=*=*=*=*");

    }

    function get_logger_ids_by_alerted_dataset_type($dataset_type_id, $alert_subject){
        $url = $this->create_url_for_alert_dataset_type_id($dataset_type_id);
        debug_log('url in get_logger_ids_by_alerted_dataset_type_id: '.$url);

        $loggers = json_decode(file_get_contents($url));
        write_to_log("type of loggers: " . gettype($loggers));

        write_to_log('Count of '.$alert_subject.' alerts before deleting duplicated: '.(string)count($loggers));


        $logger_ids = array();
        $i=0;
        $this::$original_alerts_from_plantbeat = array_merge($this::$original_alerts_from_plantbeat, $loggers);

        foreach($loggers as $logger){
            if (in_array($logger->remote_logger_id, $logger_ids) || $logger->owner_id == 1292 ) {
                unset($loggers[$i]);
            }
            else {
                array_push($logger_ids, $logger->remote_logger_id);
            }
            $i++;
        }

        write_to_log('Count of '.$alert_subject.' alerts after deleting duplicated: '.(string)count($loggers));
        $logger_ids = array_unique($logger_ids);
        debug_log($alert_subject.' alerts logger ids: '.implode(',',$logger_ids));
        return $logger_ids;
    }

    function remove_not_relevant_alerts_in_zoho($relevant_loggers_ids, $subject)
    {
        debug_log("in remove_not_relevant_alerts_in_zoho() for subject ".$subject);

        $zoho_alerts = get_all_alerts_from_zoho_by_subject($subject);

        add_seconds_to_timeout((count( $zoho_alerts ) -
                count($relevant_loggers_ids))  * 10);

        foreach ($zoho_alerts as $zoho_alert) {

            $alert_id = 			$zoho_alert->FL[0][0];
            //debug_log("alert id: ".$alert_id);
            $logger_phytech_id =	$zoho_alert->FL[1][0];
            //debug_log("logger_phytech_id: ".$logger_phytech_id);
            $task_id =				$zoho_alert->FL[2][0];
            //debug_log("task id: ".$task_id);
            $alert_subject =        $zoho_alert->FL[3][0];
            //debug_log("alert_subject: ".$alert_subject);

            $task = $task_id != null ? get_zoho_task($task_id) : null;

            if (!in_array($logger_phytech_id, $relevant_loggers_ids) && $task != null &&
                is_alert_not_in_handling($task))
            {
                delete_alert_from_zoho($alert_id);
            }
        }
        debug_log("in end of remove_not_relevant_alerts_in_zoho(".$subject.')');
    }

    public function create_alerts($loggers_ids)
    {
        foreach ($loggers_ids as $remote_logger_id) {
            write_to_log("go to creating alert manager for loggers id: " . $remote_logger_id);
            $this::creating_alert_manager($remote_logger_id, static::alert_subject);
            write_to_log("after creating alerts in end of low_battery_alerts::create_alerts");
        }
    }

    function creating_alert_manager($remote_logger_id, $alert_subject) {
        if (is_alert_in_progress_in_zoho ($remote_logger_id, $alert_subject)) {
            write_to_log ( "the '".$alert_subject."'' alert is already exists in zoho and it in progress" );
        }
        else
        {
            insert_record ( $module_name = 'Alerts',
                $recordXml = $this->create_xml_for_alert($logger_id = $remote_logger_id, $alert_subject));
        }
    }

    // help functions

    function get_last_alert_for_logger($logger_id)
    {
        $logger_alerts = array();

        foreach($this::$original_alerts_from_plantbeat as $logger_alert) {
            if ($logger_alert->remote_logger_id == $logger_id) {
                array_push($logger_alerts, $logger_alert);
            }
        }
        debug_log('logger #'.$logger_id.' alert count: '.count($logger_alerts));

        $currentLast = $logger_alerts[0];
        foreach($logger_alerts as $logger_alert)
        {
            if ($logger_alert->utc >= $currentLast->utc)
            {
                $currentLast = $logger_alert;
            }

        }
        debug_log('last info about alert exists in measurements from: '.$currentLast->utc);
        return $currentLast;
    }

    function create_xml_for_alert($logger_id, $subject) {
        $logger_in_zoho = get_logger_details_in_zoho ( $logger_id );
        write_to_log ( "after getting logger #" . $logger_id . " from zoho" );
        $url = get_plantbeat_url_row ( 'activeadmin/remote_loggers/' . $logger_id . '/status' );
        write_to_log("url to get logger details from plantbeat: ".$url);
        $logger_status = json_decode ( file_get_contents ( $url ) );

        $projects_array = array ();
        foreach ( $logger_status->projects as $project ) {
            array_push ( $projects_array, $project->id . ': ' . $project->name );
        }
        $projects_string = join ( " | \n", @$projects_array );
        write_to_log ( "projects of logger id " . $logger_id . ": " . $projects_string );

        $xml = '<CustomModule3><row no="1">' .
            '<Fl val="Projects">' . $projects_string . '</Fl>' .
            '<FL val="Logger phytech id">' . $logger_id . '</FL>' .
            '<FL val="CustomModule3 Name">' . $subject . '</FL>' .
            $this->add_special_data_for_alert_type($logger_status) .
            get_data_from_zoho_logger ( $logger_in_zoho ) . '</row></CustomModule3>';
        debug_log('xml: '.$xml);
        return $xml;
    }

    private function create_url_for_alert_dataset_type_id($dataset_type_id){
            $current_date = date('Y-m-d');
            $start_date = date('Y-m-d', strtotime($current_date . ' -1 day'));
            $end_date = date('Y-m-d', strtotime($current_date . ' +1 day'));
            $url = get_plantbeat_url_row('/activeadmin/remote_loggers/alerts').'&start='.$start_date.
                                        '&end='.$end_date.'&type_ids[0]='.$dataset_type_id.'.json';
            write_to_log('url for getting alerts of dataset type '.$dataset_type_id.': '.$url);
            return $url;
    }
}

class low_battery_alerts extends alert_base
{
    const alert_subject = 'Logger low battery';

    function alert_dataset_types_ids(){
        return array(DATASET_TYPES_IDS::BATTERY);
    }


    public function add_special_data_for_alert_type($logger_status){
        debug_log('##in add_special_data_for_alert_type##');

        $logger_alert_from_zoho = null;
        $logger_alert = parent::get_last_alert_for_logger($logger_status->id);
        return ('<FL val="Details">' .
            'Low battery value: '.$logger_alert->value .
            ' Timestamp: '.$logger_alert->utc .
            '</FL>');

        debug_log('##in end of add_special_data_for_alert_type##');
    }
}

class problematic_dendrometer_alerts extends alert_base
{
    const alert_subject = 'Invalid dendrometer values';

    function alert_dataset_types_ids(){
      return array(DATASET_TYPES_IDS::DENDROMETER);
    }

    function add_special_data_for_alert_type($logger_status){
        debug_log('##in add_special_data_for_alert_type##');

        $logger_alert = parent::get_last_alert_for_logger($logger_status->id);

        return ('<FL val="Details">' .
            'Dendrometer value: '.$logger_alert->value . ' | ' .
            'Timestamp: '.$logger_alert->utc . ' | ' .
            'Sensor id: '.$logger_alert->sensor_id .
            '</FL>');

        debug_log('##in end of add_special_data_for_alert_type##');
    }
}

class problematic_stem_diameter_alerts extends alert_base
{
    const alert_subject = 'Invalid stem diameter values';

    function alert_dataset_types_ids()
    {
        return array(DATASET_TYPES_IDS::STEM_DIAMETER_SENSOR_TYPE_SD_REV2,
            DATASET_TYPES_IDS::STEM_DIAMETER_SENSOR_TYPE_SD_56R);
    }

    function add_special_data_for_alert_type($logger_status){
        debug_log('##in add_special_data_for_alert_type##');

        $logger_alert = parent::get_last_alert_for_logger($logger_status->id);

        return ('<FL val="Details">' .
            'Stem diameter value: '.$logger_alert->value . ' | ' .
            'Timestamp: '.$logger_alert->utc . ' | ' .
            'Sensor id: '.$logger_alert->sensor_id .
            '</FL>');

        debug_log('##in end of add_special_data_for_alert_type##');
    }
}
