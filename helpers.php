<?php
include_once("config.php");

function get_plantbeat_url_row($route){
    return $GLOBALS['server_string'].$route.'?'.$GLOBALS['credentials'];
}

function execute_post_request($req_for_post, $postData)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_URL, $req_for_post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $response = curl_exec($ch);

    if (curl_errno($ch)) {
        //$output = 'error';
        write_to_log('error accured in post http ' . curl_errno($ch) . '<br>');
    }

    curl_close($ch);

    return $response;
}

function get_element_from_xml($xml_object, $element_name)
{
    return $xml_object->getElementsByTagName($element_name)->item(0)->nodeValue;
}

function is_invoice_chul($module, $element_to_append)
{
    return $module == 'Invoices' && $element_to_append->getElementsByTagName('Currency')->item(0)->nodeValue != 'ILS';
}


function update_xml_for_priority($module, $_phytech_id, $element_to_append, $xml_from_web)
{
    $file_name = is_invoice_chul($module, $element_to_append) ?
        'xml/Invoices_chul.xml' : 'xml/' . $module . ".xml";

    debug_log("file name = " . $file_name . '<br>');

    if ($module == 'Invoices')
    {
        debug_log('type of element to append: '.gettype($element_to_append));
        $element_to_append = set_invoice_product_sku_as_product_id($element_to_append);
    }
    if (is_invoice_chul($module, $element_to_append)) {
        $element_to_append = set_currency_before_products($element_to_append);
    }
    if ($module == 'Products'){
        $element_to_append = set_sku_as_product_id($element_to_append);
    }
    if ($module == 'Accounts'){
        $element_to_append = remove_zip_code_zero($element_to_append);
    }
    $file_subject = "updating_xml";

    sleep(rand(3,11));
    while (file_exists(get_lock_file_name($file_subject))){
        sleep(rand(3, 11));
        if (file_exists(get_lock_file_name($file_subject))){
            write_to_log("Waiting because there is " . get_lock_file_name($file_subject) . " file...");
        }
    }
    
    delete_previous_lock_file($file_subject);
    
    create_lock_file($file_subject);
    write_to_log('lock file ' . $file_subject . ' created ');

    if ($_phytech_id == '-1') {
        debug_log("phytech id is -1");
        if (file_exists($file_name)) {
//need to read file into xml object, read file from server, if id from server does not match any id in file from disk, append child.
            $doc_from_disc = new DOMDocument();
            $doc_from_disc->load($file_name);
            //see if element_to_append exist already in xml_from_disc
            switch ($module) {
                case "Invoices":
                    $id_name = 'INVOICEID';
                    debug_log($id_name . '<br>');
                    break;
                case "Accounts":
                    $id_name = 'ACCOUNTID';
                    debug_log($id_name . '<br>');
                    break;
                case "Products":
                    $id_name = 'PRODUCTID';
                    debug_log($id_name . '<br>');
                    break;
                case "Contacts":
                    $id_name = 'CONTACTID';
                    debug_log($id_name . '<br>');
                    break;
                default:
                    debug_log("Your favorite color is neither red, blue, or green!");
            }
            $id_from_web = $element_to_append->getElementsByTagName($id_name)->item(0)->nodeValue;
            debug_log("id_from_web = $id_from_web <br>");
            $rows_from_disc = $doc_from_disc->getElementsByTagName('response')->item(0)->getElementsByTagName('result')->item(0)->getElementsByTagName($module)->item(0)->getElementsByTagName('row');
            $parent = $doc_from_disc->getElementsByTagName('response')->item(0)->getElementsByTagName('result')->item(0)->getElementsByTagName($module)->item(0);
            foreach ($rows_from_disc as $row) {
                $id_from_disc = $row->getElementsByTagName($id_name)->item(0)->nodeValue;
                debug_log($id_from_disc);
                if ($id_from_disc == $id_from_web) {
                    //need to remove existing row
                    $parent->removeChild($row);
                    break;
                }
            }
            //need to append row
            $element_to_append = $doc_from_disc->importNode($element_to_append, true);
            $parent->appendChild($element_to_append);
            $doc_from_disc->save($file_name);
        } else {//file does not exist, so simply save data to file
            $xml_from_web->saveXML($file_name);
            $doc_from_disc = new DOMDocument();
            $doc_from_disc->load($file_name);
            if($doc_from_disc){
                $row_from_disc = $doc_from_disc->getElementsByTagName('response')->item(0)->getElementsByTagName('result')->item(0)->getElementsByTagName($module)->item(0)->getElementsByTagName('row')->item(0);
                $parent = $doc_from_disc->getElementsByTagName('response')->item(0)->getElementsByTagName('result')->item(0)->getElementsByTagName($module)->item(0);
                $parent->removeChild($row_from_disc);
                $element_to_append = $doc_from_disc->importNode($element_to_append, true);
                $parent->appendChild($element_to_append);
                $doc_from_disc->save($file_name);
                //$xml_from_web->asXML($file_name);
                debug_log("OK");
            }
        }
        if ($module == 'Invoices'){
            write_to_log('go to updating invoice field "integrated" invoice id: '.$id_from_web);
            update_zoho_about_invoice_integration($id_from_web);
        }
        //$doc_from_disc->save($file_name);
    }
    else {
        debug_log("The module called accounts, or phytech id != -1. The synchronization with Priority doesn't needed");
    }

    write_to_log("deleting lock file");
    delete_previous_lock_file($file_subject);
}

function update_zoho_about_invoice_integration($suffix_invoice_id)
{
    $full_zoho_id = $GLOBALS['prefix_zoho_id'].$suffix_invoice_id;
    write_to_log('full of invoice id: ' . $full_zoho_id);
    $output = update_zoho_field('true', $full_zoho_id, 'Invoices', 'Integrated');
    debug_log('response from zoho about updating status: <br>'.$output);
}

function set_currency_before_products($element_to_append)
{
    if (gettype($element_to_append) == 'string')
    {
        debug_log($element_to_append);
    }
    debug_log("type of element to append: " . gettype($element_to_append));
    $subject_element = $element_to_append->getElementsByTagName('Subject')->item(0);
    $currency_element = $element_to_append->getElementsByTagName('Currency')->item(0);
    $element_to_append->removeChild($currency_element);
    $element_to_append->insertBefore($currency_element,$subject_element);
    return $element_to_append;
    //debug_log("content: " . $element_to_append);
};

function remove_zip_code_zero($element_to_append)
{
    $zip_code_element = $element_to_append->getElementsByTagName('Billing_Zip_Code')->item(0);
    debug_log('billing zip code: '.$zip_code_element->nodeValue);
    if ($zip_code_element->nodeValue == '0'){
        $zip_code_element->nodeValue = '';
    }
    $element_to_append->removeChild($zip_code_element);
    return $element_to_append;
};

function send_response()
{
    $response = new \Phalcon\Http\Response();
    $response->setStatusCode(200, "OK");
    $response->setContent("<html><body>HelloBello</body></html>");
    $response->send();
}

function delete_previous_lock_file($name){
    foreach (glob("lock_".$name."*") as $filename) {
        echo "Deleting previous lock file: " . $filename;
        unlink($filename);
    }
}

function get_lock_file_name($subject)
{
    $file_name = "lock_" . $subject . date("Y-m-d-H.") . (string)intval(date("i") / 10);
    debug_log("lock file name by function:".$file_name);
    return $file_name;
}

function create_lock_file($subject)
{
    $lock_file_name = get_lock_file_name($subject);
    $lock_file = fopen($lock_file_name, "w");
    fclose($lock_file);
}

function is_active_product($module, $method)
{
    return ($module == 'Products' && $method == 'ActiveProductCreatedOrEdited');
}

function is_not_active_product($module, $method)
{
    return !is_active_product($module, $method);
}

function set_sku_as_product_id($product_element)
{
    $sku_element = $product_element->getElementsByTagName('SKU')->item(0);
    $product_id_element = $product_element->getElementsByTagName('PRODUCTID')->item(0);
    $product_id_element->nodeValue = $sku_element->nodeValue;
    return $product_element;
    //debug_log("content: " . $element_to_append);
}

function set_invoice_product_sku_as_product_id($element_to_append)
{
    debug_log('type of element to append: '.gettype($element_to_append));
    $product_elements = $element_to_append->getElementsByTagName('product');
    foreach ($product_elements as $product_element)
    {
        $zoho_product_id = $product_element->getElementsByTagName('Product_Id')->item(0)->nodeValue;
        $product_domdocument = get_product_domdocument_from_zoho_by_id($zoho_product_id);

        $catalog_number = $product_domdocument->getElementsByTagName('SKU')->item(0)->nodeValue;
            //$product_catalog_id = function
        $product_element->getElementsByTagName('Product_Id')->item(0)->nodeValue = $catalog_number;
    }

    return $element_to_append;
}

function get_product_domdocument_from_zoho_by_id($product_id){
    $url = get_prefix_zoho_request ( 'Products', 'getRecordById' ) . '&id='.$product_id;
    return convertXmlToDOMDocument(simplexml_load_file($url), 'Products');
}

function activate_product($zoho_id, $phytech_id)
{
    $logger = get_logger_status_from_plantbeat_json($phytech_id);

    if ($logger == null){
        $message = "The logger #".$phytech_id." doesn't exists in plantbeat";
    }
    else if ($logger->activated){
        $message = "The logger #".$phytech_id." is already activated";
    }
    else if (!$logger->tested){
        $message = "The logger #".$phytech_id." is untested. it disable to be activated";
    }
    else
    {
        $urlPath = $GLOBALS['server_string'] . 'activeadmin/remote_loggers/'.$phytech_id.'/activate.json';
        $plantbeat_response = httpPost($urlPath, get_plantbeat_credentials(), 'PUT', null);
        debug_log('response from plantbeat after activating: '.$plantbeat_response);
        updateZohoAboutPhytechStatus($zoho_id, 'Products', $plantbeat_response,$phytech_id);
        if (get_logger_status_from_plantbeat_json($phytech_id)->activated) {
            $message = "Activation finished successfully. refresh the page to show product phytech status";
        }
        else {
            $message = "Activation failed";
        }
    }

    write_to_log('<br><br>======================================================================================<br>'.
                $message.
                '<br>======================================================================================<br>');
}

function get_plantbeat_credentials(){
    $phytech_arr =  [];
    $phytech_arr['api_token'] = $GLOBALS['phytech_admin_token'];
    $phytech_arr['user_id'] = $GLOBALS['phytech_admin_id'];
    return $phytech_arr;
}
function dispatcher() {
    load_request_payload_data ();
    $method = $_GET ['METHOD'];
    switch ($method) {
        case UPDATING_ZOHO_METHODS::LOAD_ALERTS :
            if (is_updating_alerts_can_run())
            {
                create_lock_file(UPDATING_ZOHO_METHODS::LOAD_ALERTS);

                run_file_in_other_process();
            }
            break;
        case ENDPOINTS::ACTIVATE_PRODUCT :
            activate_product($_GET['ZOHO_ID'], $_GET['PHYTECH_ID']);
			break;
        case ENDPOINTS::LOAD_MOBILE_USAGE :
            load_mobile_usage();
			break;
        default :
            write_to_log ( "Unknown method " . $method );
    }
}
?>
