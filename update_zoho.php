<?php
include_once ("config.php");
include_once ("helpers.php");
include_once ("alerts.php");
include_once ("../google_analytics_zoho_integration/index.php");

/**
 * Created by PhpStorm.
 * User: User
 * Date: 30/07/2015
 * Time: 11:37
 */
class UPDATING_ZOHO_METHODS {
	const LOAD_ALERTS = "load_alerts";
}

class ENDPOINTS {
	const ACTIVATE_PRODUCT = "ActivateProduct";
        const LOAD_MOBILE_USAGE = "load_mobile_usage";
}

class ZOHO_MODULES_NAMES {
	const ALERTS = 'CustomModule3';
}


$GLOBALS ['cmd_parameter_for_creating_alerts'] = "create_alerts";
$GLOBLLS ['file_name_loading_alerts_history'] = "load_alert_run_history.html";

function get_prefix_zoho_request($module, $api_function) {
	return 'https://crm.zoho.com/crm/private/xml/' . $module . '/'.$api_function.'?authtoken=' . $GLOBALS ['authToken'] . '&scope=crmapi';
}			

function insert_record($module_name, $recordXml) {
	$postData = 'authtoken=' . $GLOBALS ['authToken'] . '&scope=crmapi&xmlData=' . $recordXml;
	$req_for_post = 'https://crm.zoho.com/crm/private/xml/'.$module_name.'/insertRecords?';

	$output = execute_post_request ( $req_for_post, $postData );
	
	write_to_log ( 'response from zoho : ' . $output . '<br>' );
	write_to_log ( 'req sent as post to zoho = ' . $req_for_post . $postData . '<br>' );
}

function get_logger_details_in_zoho($logger_id) {
	write_to_log ( 'in get_logger_details_in_zoho(' . $logger_id . ')' );
	
	$url = get_prefix_zoho_request ( 'Products', 'searchRecords' ) . '&criteria=(Product code:' . $logger_id . ')';
	debug_log ( 'getting product details by logger id from zoho. Full url: ' . $url );
	
	$xml_from_web = simplexml_load_file ( $url );
														 
	$product = convertXmlToDOMDocument ( $xml_from_web, 'Products' );
	debug_log ( "after getting product from zoho" );
	
	return $product;
}

function get_battery_and_rssi($datasets) {
	$arr = array ();
	foreach ( $datasets as $dataset ) {
		debug_log ( 'Title: ' . $dataset->title );
		$arr [$dataset->title] = $dataset->measurements [0]->value;
	}
	$arr ['last_contact_time'] = $datasets [0]->measurements [0]->local;
	return $arr;
}

function get_logger_status_from_plantbeat_json($logger_id){
	$url = get_plantbeat_url_row ( 'activeadmin/remote_loggers/' . $logger_id . '/status' );
	write_to_log("url to get logger details from plantbeat: ".$url);
	return json_decode ( file_get_contents ( $url ) );
}

function create_xml_for_mobile_usage($record)
{	
	debug_log('contact_id: '.$record['contact_id']);
	debug_log('company_id: '.$record['company_id']);
	$xml = '<CustomModule6><row no="1">' . 
		'<FL val="Num of sessions">' . 		$record['sessions_count'] . '</FL>' .
		'<FL val="Session date">' . 		get_formatted_date($record['date']) . '</FL>' . 
		'<FL val="Country">' . 				$record['country']	 . '</FL>' . 
		'<FL val="Contact_ID">' . 			$record['contact_id'] . '</FL>' . 
		'<FL val="Company_ID">' . 			$record['company_id'] . '</FL>' . 
		'<FL val="CustomModule6 Name">' . 	$record['mobile_type'] . '</FL>' . 
		'</row></CustomModule6>';
	debug_log('xml: '.$xml);
	return $xml;
}

function get_formatted_date($orig_date)
{
	return substr($orig_date,0,4)."-".
		substr($orig_date,4,2)."-".substr($orig_date, 6, 2);
}

function create_xml_for_alert($logger_id, $alert_subject) {
	$logger_in_zoho = get_logger_details_in_zoho ( $logger_id );
	write_to_log ( "after getting logger #" . $logger_id . " from zoho" );
	$logger_status = get_logger_status_from_plantbeat_json($logger_id);
	$battery_and_rssi = get_battery_and_rssi ( $logger_status->datasets );
	
	foreach ( $battery_and_rssi as $k => $v ) {
		debug_log ( 'key = ' . $k . '<br>' );
		debug_log ( 'v1 = ' . $v . '<br>' );
	}
	
	debug_log ( "Battery: " . $battery_and_rssi ["Logger Battery"] );
	
	$projects_array = array ();
	foreach ( $logger_status->projects as $project ) {
		array_push ( $projects_array, $project->id . ': ' . $project->name );
	}
	$projects_string = join ( " | \n", @$projects_array );
	write_to_log ( "projects of logger id " . $logger_id . ": " . $projects_string );
	
	debug_log('[$GLOBALS["current_alerts_type"]: '.$GLOBALS["current_alerts_type"]);

	$xml = '<CustomModule3><row no="1">' . 
		'<FL val="Battery">' . $battery_and_rssi ["Logger Battery"] . '</FL>' . 
		'<FL val="RSSI">' . $battery_and_rssi ["RSSI"] . '</FL>' . 
		'<Fl val="Projects">' . $projects_string . '</Fl>' . 
		'<FL val="Logger phytech id">' . $logger_id . '</FL>' . 
		'<FL val="Last contact ts">' . $battery_and_rssi ['last_contact_time'] . '</FL>' . 
		'<FL val="CustomModule3 Name">' . $alert_subject . '</FL>' .
		get_data_from_zoho_logger ( $logger_in_zoho ) . '</row></CustomModule3>';
	debug_log('xml: '.$xml);
	return $xml;
}

function get_data_from_zoho_logger($logger_in_zoho) {
	if ($logger_in_zoho != null) {
		debug_log("logger in zoho is not null");
		return '<FL val="Remote Logger_ID">' . get_element_from_xml ( $logger_in_zoho, 'PRODUCTID' ) . '</FL>' . 
			   '<FL val="Company_ID">' . get_element_from_xml ( $logger_in_zoho, 'Company_ID' ) . '</FL>';
	} else {
		debug_log("logger in zoho is null");
		return '';
	}
}

function get_non_reporting_loggers_from_plantbeat() {
	$url = get_plantbeat_url_row('activeadmin/remote_loggers/disconnected_loggers');
	write_to_log("url to get non reporting alerts: " . $url);
	$loggers = json_decode(file_get_contents($url));
	write_to_log("Count of non reporting loggers: ".(string)count($loggers));
	
	$logger_ids = array();
	debug_log("count loggers before filtering 'system dead systems': " . (string)count($loggers));
	foreach($loggers as $logger){
		if ($logger->owner_id != 1292) // system_dead_systems company id = 1292
		{
			array_push($logger_ids, $logger->id);
		}
	}
	debug_log("count loggers after filtering 'system dead systems': " . (string)count($logger_ids));
	return $logger_ids = array_unique($logger_ids);
	debug_log("count loggers after uniquing: " . (string)count($logger_ids));
}

function get_alerts_from_zoho_from_do_index($fromIndex, $toIndex, $module_name, $alerts_subject){
	$criteria = ZOHO_MODULES_NAMES::ALERTS. ' Name:'.$alerts_subject;
	$url = get_prefix_zoho_request($module_name, 'searchRecords') .
									'&fromIndex=' . (string)$fromIndex . 
									'&toIndex=' . (string)$toIndex .
									'&selectColumns=CustomModule3(id,Task ID,Logger phytech id)' .
									'&criteria=('.$criteria.')';
			
	debug_log('url to get alerts:<br>' . $url);	
	$xml_from_web = simplexml_load_file($url);
	debug_log('after doing request');

	if (gettype($xml_from_web) == "boolean"){
		return array();
	}

	$y =  $xml_from_web->xpath('//row');
	debug_log("Type of y: ".gettype($y));
	debug_log('count of $y'.(string)count($y));
	return $y;
}


function get_all_alerts_from_zoho_by_subject($alerts_subject){
	$alerts_from_zoho = array();
	$additional_alerts = array();
	$from_index = 1;

	do {
		$additional_alerts = get_alerts_from_zoho_from_do_index($from_index=$from_index,
																$do_index=$from_index + 199,
																$module_name=ZOHO_MODULES_NAMES::ALERTS,
																$alerts_subject=$alerts_subject);
		$alerts_from_zoho = array_merge($alerts_from_zoho, $additional_alerts);
		$from_index = $from_index + 200;
	} while (count($additional_alerts) == 200);

	return $alerts_from_zoho;
}


function remove_not_relevant_alerts_in_zoho($relevant_loggers_ids){
	debug_log("in remove_not_relevant_alerts_in_zoho()");
	
	$zoho_alerts = get_all_alerts_from_zoho_by_subject(ALERTS_TYPES_TITLE::NOT_REPORTING);
	
	add_seconds_to_timeout(count( $zoho_alerts ) * 100);
	
	foreach ($zoho_alerts as $zoho_alert) {
		
		$alert_id = 			$zoho_alert->FL[0][0];
		debug_log("alert id: ".$alert_id);
		$logger_phytech_id =	$zoho_alert->FL[1][0];		
		debug_log("logger_phytech_id: ".$logger_phytech_id);
		$task_id =				$zoho_alert->FL[2][0];
		debug_log("task id: ".$task_id);

		$task = $task_id != null ? get_zoho_task($task_id) : null;

		if (!in_array($logger_phytech_id, $relevant_loggers_ids) &&
					  is_alert_not_in_handling($task))
		{
			//delete_alert_pending_task($task);
			delete_alert_from_zoho($alert_id);
		}
		$dom = new DOMDocument();		
	}
}

function delete_alert_from_zoho($alert_id){
	$url = get_prefix_zoho_request('CustomModule3', 'deleteRecords')."&id=".$alert_id;
	write_to_log ("url for deleting alert ".$alert_id.": ".$url);
	$response = simplexml_load_file($url);
	debug_log("response for deleting: ".json_encode($response));
}

function delete_alert_pending_task($task){
	if ($task->getElementsByTagName('Due Date')->length == 0){
		$url = get_prefix_zoho_request('Tasks', 'deleteRecords')."&id=".$task->id;
		write_to_log ("url for deleting task ".$task->id.": ".$url);
		$response = simplexml_load_file($url);
		debug_log("response for deleting task: ".json_encode($response));
	}
}

function is_alert_not_in_handling($task){
	return $task == null ||
		!in_array(get_task_status($task), array('Handled', 'Completed')); 		    
}


function update_zoho_on_non_reporting_loggers() {
	$GLOBALS['current_alerts_type'] = ALERTS_TYPES_TITLE::NOT_REPORTING;
	write_to_log ("in Update_zoho_on_non_reporting_loggers");
	
	$loggers_ids = get_non_reporting_loggers_from_plantbeat();
	write_to_loading_alerts_log("after getting non reporting logger ids from plantbeat");

	add_seconds_to_timeout(count($loggers_ids) * 10);

	remove_not_relevant_alerts_in_zoho($relevant_loggers_ids=$loggers_ids);
	write_to_loading_alerts_log("Non relevant non reporting logger alerts in zoho was removed");

	create_alerts($loggers_ids, ALERTS_TYPES_TITLE::NOT_REPORTING);
	write_to_loading_alerts_log("after creating new non reporting logger alerts");
}



function create_alerts($loggers_ids, $alert_subject) {
	foreach ($loggers_ids as $remote_logger_id) {
		write_to_log("go to creating alert manager for loggers id: " . $remote_logger_id);
		creating_alert_manager($remote_logger_id, $alert_subject);
		write_to_log("after creating alerts");
	}
}

function add_seconds_to_timeout($timeout_in_minutes){
	$prev_seoncds_timeout = ini_get('max_execution_time');
	$timeout_in_minutes = $timeout_in_minutes * 60;
	write_to_log ( 'set max_execution_time -> ' .
		(string)($timeout_in_minutes) .' + '.(string)$prev_seoncds_timeout);

	ini_set ( 'max_execution_time', $timeout_in_minutes + $prev_seoncds_timeout);
	write_to_log ('new max execution time: ' . (string)ini_get('max_execution_time'));
}


function remove_problematic_values_from_simplexml($simplexmlElement, $value_name)
{
	if (gettype(@$simplexmlElement) != "boolean"){
		$rows = $simplexmlElement->xpath("//FL[@val='".$value_name."']");
		foreach ($rows as $node){
			$dom=dom_import_simplexml($node);
			$dom->parentNode->removeChild($dom);
		}
	}
}

function get_alert_domdocument_from_zoho($logger_id, $alert_message) {
	debug_log("### in get_alert_domdocument_from_zoho(.$logger_id.$alert_message.') ###");

	$url = get_prefix_zoho_request ( 'CustomModule3', 'searchRecords' ) . '&criteria=('.
			'(logger phytech id:' . $logger_id . ")AND(CustomModule3 Name:" . $alert_message . '))';
	debug_log ( 'getting '.$alert_message.' alert by logger id from zoho. Full url: ' . $url );

	$xml_from_web = simplexml_load_file($url);

	# link to task contains character '&' and function 'convertXmlToDOMDocument' failed therefore
	remove_problematic_values_from_simplexml($xml_from_web, 'Link_to_task');

	$alert_domdocument = convertXmlToDOMDocument ( $xml_from_web, 'CustomModule3' );
	
	if ($alert_domdocument == null) # if no "row" element in xml response = no data
	{
		debug_log("NO PREVIOUS ALERTS");
	}
	debug_log("in end of get_alert_domdocument_from_zoho('.$logger_id.,'$alert_message')");
	return $alert_domdocument;
}


function is_alert_in_progress_in_zoho($remote_logger_id, $alert_subject) {
	$alerts_in_zoho = get_alert_domdocument_from_zoho (
			$logger_id = $remote_logger_id, 
			$alert_message = $alert_subject);
	
	return ($alerts_in_zoho != null &&
			(any_alert_without_task($alerts_in_zoho) || any_task_not_completed($alerts_in_zoho)));
}


function any_alert_without_task($alerts_domElement){
	debug_log("###in any_alert_without_task###");
	$alerts_root = $alerts_domElement->parentNode;
	
	$alerts_count = $alerts_root->getElementsByTagName("row")->length;
	debug_log("alerts count: " . $alerts_count);
	$tasks_count = $alerts_root->getElementsByTagName("Task_ID")->length;
	debug_log("alerts with 'Task id' parameter: ".(string)$tasks_count);

	debug_log("###in end of any_alert_without_task###");
	return $tasks_count < $alerts_count;
}

function any_task_not_completed($alerts_domelement){
	$alerts_root = $alerts_domelement->parentNode;
	$tasks_ids_elements = $alerts_root->getElementsByTagName("Task_ID");
	foreach ($tasks_ids_elements as $task_id_element){
		$task = get_zoho_task($task_id_element->nodeValue);
		$task_status = get_task_status($task);		
		if (!in_array($task_status, array('Completed', 'Handled'))) {
			debug_log("task is in progress or not started");
			return true;
		}
	}
	return false;
}


function get_zoho_task($task_id){
	$url = get_prefix_zoho_request ( 'Tasks', 'getRecordById' ) . '&id='.$task_id;
	debug_log ( 'getting task details by task id from zoho. Full url: ' . $url );
	$xml_from_web = simplexml_load_file ( $url );
	return convertXmlToDOMDocument ( $xml_from_web, 'Tasks' );
}

function get_task_status($task) {
	if ($task == null) {
		return null;
	}
	else
		{
		$status = $task->getElementsByTagName('Status')->item(0)->nodeValue;
		debug_log("Status of task id " . $task->getElementsByTagName('ACTIVITYID')->item(0)->nodeValue . ": " . $status);
		return $status;
	}
}


function creating_alert_manager($remote_logger_id, $alert_subject) {

	if (is_alert_in_progress_in_zoho ($remote_logger_id, $alert_subject)) {
		write_to_log ( "the 'non reporting' alert is already exists in zoho and it in progress" );
	} 
	else 
	{
		insert_record ( $module_name = 'Alerts',
			$recordXml = create_xml_for_alert($logger_id = $remote_logger_id, $alert_subject ) );
	}
}

function run_file_in_other_process() {
	write_to_log ( "run update_zoho.php in other process" );
	shell_exec ( 'php update_zoho.php ' . $GLOBALS ['cmd_parameter_for_creating_alerts'] . '> debug_log.html &' );
	write_to_log ( "handling for loggers finished in the main process" );
}

function load_request_payload_data() {
	$putfp = fopen ( 'php://input', 'r' );
	$putdata = '';
	while ( $data = fread ( $putfp, 1024 ) ) {
		$putdata .= $data;
	}
	fclose ( $putfp );
	$GLOBALS ['request_payload_data'] = json_decode ( $putdata );
}

function load_mobile_usage()
{
	$email_sessions = get_usage_of_yesterday();
	write_to_log('$session count: '.count($email_sessions));
	/*foreach ($email_sessions as $record)
	{
		add_mobile_usage_record($record);
	}*/
	add_mobile_usage_record($email_sessions[0]);
}

function add_mobile_usage_record($record)
{
	$contact = get_zoho_contact_by_email($record["email"]);
	if ($contact == null)
	{
		debug_log("contact with email ".$record["email"].' doesn\'t exists in zoho');
	}
	else
	{
		$record['company_id'] = $contact->getElementsByTagName('ACCOUNTID')->item(0)->nodeValue;
		$record['contact_id'] = $contact->getElementsByTagName('CONTACTID')->item(0)->nodeValue;
		insert_record ( $module_name = 'CustomModule6',
			$recordXml = create_xml_for_mobile_usage($record));
	}
}

function get_zoho_contact_by_email($email) {
	write_to_log ( 'in get_zoho_contact_by_email(' . $email . ')' );
	
	$url = get_prefix_zoho_request ( 'Contacts', 'searchRecords' ) . '&criteria=(Email:' . $email . ')';
	debug_log ( 'getting product details by logger id from zoho. Full url: ' . $url );
	
	$xml_from_web = simplexml_load_file ( $url );
														 
	$contact = convertXmlToDOMDocument ( $xml_from_web, 'Contacts' , true);
	debug_log ( "after getting product from zoho" );
	
	return $contact;
}

function is_updating_alerts_can_run(){
	
	$lock_file_name = get_lock_file_name(UPDATING_ZOHO_METHODS::LOAD_ALERTS);
    
    if (file_exists(get_lock_file_name(UPDATING_ZOHO_METHODS::LOAD_ALERTS))){
    	write_to_log("''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''");
        write_to_log("Updating alerts running now. Try run again more some minutes");
        write_to_log("''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''");
        return false;
    }
    else
    {
		write_to_log("''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''");
		write_to_log("Updating alerts started successfully.");
		write_to_log("''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''");
		delete_previous_lock_file(UPDATING_ZOHO_METHODS::LOAD_ALERTS);
    	return true;
    }    
}

if (isset ( $argv )) {
	write_to_log ( "update_zoho script called with arguments" );
	
	if (in_array ( $GLOBALS ['cmd_parameter_for_creating_alerts'], $argv )) {
		$_GET['METHOD'] = $GLOBALS ['cmd_parameter_for_creating_alerts'];
		write_to_log ( "run update_zoho_on_non_reporting_loggers()" );

		add_seconds_to_timeout(10000);

		$alerts_types = array(new problematic_stem_diameter_alerts, new problematic_dendrometer_alerts, new low_battery_alerts);
		foreach($alerts_types as $alerts_subject) {
			add_seconds_to_timeout(10000);
			$alerts_subject->run();
		}

		write_to_loading_alerts_log("*=*=*=*= updating zoho non reporting loggers alerts was started... *=*=*=*=*=");
		update_zoho_on_non_reporting_loggers ();
		write_to_loading_alerts_log("*=*=*=*= updating zoho non reporting loggers alerts was finished... *=*=*=*=*=");

		write_to_log ( "createing alerts for non reporting loggers finished successfuly" );
		delete_previous_lock_file(UPDATING_ZOHO_METHODS::LOAD_ALERTS);
	}
} else {
	write_to_log ( "update_zoho script called from another file" );
}
?>
